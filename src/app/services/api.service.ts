import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { ConstantsService } from './constants.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private tokenStorageService: TokenService, private constants: ConstantsService) { }


  //Login
  login(credentials: { username: any; password: any; }): Observable<any> {
    return this.http.post(`${baseUrl}/api/users/v1/login`, {
      username: credentials.username,
      password: credentials.password
    }, { headers: this.constants.emptyHeaders });
  }

}
