import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { ConstantsService } from './constants.service';
import { TokenService } from './token.service';


@Injectable({
  providedIn: 'root'
})
export class AtmService {
  
  constructor(private http: HttpClient, private tokenStorageService: TokenService, private constants: ConstantsService) { }

  public getAllAtm(): Observable<any> {
    return this.http.get(`${baseUrl}/api/atm/v1/list-all`, { headers: this.constants.getHeaders() });
  }

  public searchAtm(searchKey :any, searchValue :any, searchType :any): Observable<any> {
    return this.http.get(`${baseUrl}/api/atm/v1/search-by-key-value-type/${searchKey}/${searchValue}/${searchType}`, { headers: this.constants.getHeaders() });
  }

  public postAtm(atmForm: any): Observable<any> {
    let data = {
      "atmName": atmForm.atmName,
      "atm_ID_Code": atmForm.atm_ID_Code,
      "city": atmForm.city,
      "cityCode": "",
      "clientGuid": atmForm.clientGuid,
      "latitude": "",
      "longitude": "",
      "province": atmForm.province,
      "streetAddress1": atmForm.streetAddress1,
      "streetAddress2": atmForm.streetAddress2,
      "suburb": atmForm.suburb
    };

    return this.http.post(`${baseUrl}/api/atm/v1/create`, data, { headers: this.constants.getHeaders() });
  }

  public updateAtm(atmForm: any): Observable<any> {
    let data = {
      "atmId": atmForm.atmId,
      "atmName": atmForm.atmName,
      "atm_ID_Code": atmForm.atm_ID_Code,
      "city": atmForm.city,
      "cityCode": "",
      "clientGuid": atmForm.clientGuid,
      "atmGuid": atmForm.atmGuid,
      "latitude": "",
      "longitude": "",
      "province": atmForm.province,
      "streetAddress1": atmForm.streetAddress1,
      "streetAddress2": atmForm.streetAddress2,
      "suburb": atmForm.suburb
    };

    console.log("DATA:", data);

    return this.http.put(`${baseUrl}/api/atm/v1/update`, data, { headers: this.constants.getHeaders() });
  }

  deleteAtm(atmForm: any): Observable<any>{
    return this.http.delete(`${baseUrl}/api/atm/v1/delete/${atmForm.atmGuid}`, { headers: this.constants.getHeaders() });
  }
}
