import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { ConstantsService } from './constants.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private http: HttpClient, private tokenStorageService: TokenService, private constants: ConstantsService) { }

  //Get all users
  public getAllUsers(): Observable<any> {
    return this.http.get(`${baseUrl}/api/users/v1/list-all-users`, { headers: this.constants.getHeaders() });
  }
//
  searchUsers(searchKey: any, seachValue: any, searchTypetype: any): Observable<any> {
    return this.http.get(`${baseUrl}/api/users/v1/search-user-by-key-value-type/${searchKey}/${seachValue}/${searchTypetype}`, { headers: this.constants.getHeaders() });
  }

  searchUserByGuid(userGuid: any): Observable<any> {
    return this.http.get(`${baseUrl}/api/users/v1/search-user-by-guid/${userGuid}`, { headers: this.constants.getHeaders() });
  }

  deleteUser(userForm: any): Observable<any> {
    return this.http.delete(`${baseUrl}/api/users/v1/delete-user/${userForm.userGuid}`, { headers: this.constants.getHeaders() });
  }
  
  public postUser(userForm: any): Observable<any> {
    let data = {
      "address": {
        "addressLine1": "",
        "addressLine2": "",
        "addressLine3": "",
        "addressPostalCode": "",
        "province": userForm.province
      },
      "contact": {
        "contactType": "Personal",
        "email": userForm.email,
        "mobile": userForm.mobile
      },
      "user": {
        "firstName": userForm.firstName,
        "lastName": userForm.lastName,
        "userType": userForm.userType,
        "username": userForm.email
      }
    };
    return this.http.post(`${baseUrl}/api/users/v1/registration`, data, { headers: this.constants.getHeaders() });
  }

  public changePassword(email: any, newPassword: any): Observable<any> {
    let data = {
      "email": email,
      "newPassword": newPassword,
      "otp": "123"
    };
    return this.http.post(`${baseUrl}/api/users/v1/change-password`, data, { headers: this.constants.getHeaders() });
  }

  public updateUser(userForm: any): Observable<any> {
    let data = {
      "address": {
        "addressLine1": "",
        "addressLine2": "",
        "addressLine3": "",
        "addressPostalCode": "",
        "province": userForm.province
      },
      "contact": {
        "contactType": "Personal",
        "email": userForm.email,
        "mobile": userForm.mobile
      },
      "user": {
        "userGuid": userForm.userGuid,
        "firstName": userForm.firstName,
        "lastName": userForm.lastName,
        "userType": userForm.userType,
        "username": userForm.email
      }
    };
    return this.http.put(`${baseUrl}/api/users/v1/update-user`, data, { headers: this.constants.getHeaders() });
  }
}
