import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { ConstantsService } from './constants.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class CleaningLogService {

  constructor(private http: HttpClient, private tokenStorageService: TokenService, private constants: ConstantsService) { }

  //Get today's live logs
  public getLiveLogs(): Observable<any> {
    return this.http.get(`${baseUrl}/api/cleaning/v1/list-all-today`, { headers: this.constants.getHeaders() });
  }

  //Get all cleaning logs
  public getAllCleaningLogs(): Observable<any> {
    return this.http.get(`${baseUrl}/api/cleaning/v1/list-all`, { headers: this.constants.getHeaders() });
  }

  //search cleaning log
  public searchCleaningLogs(key: any, value: any, type: any): Observable<any> {
    return this.http.get(`${baseUrl}/api/cleaning/v1/search-by-key-value-type/${key}/${value}/${type}`, { headers: this.constants.getHeaders() });
  }

  public getPersonalStats(userGuid: any): Observable<any> {
    return this.http.get(`${baseUrl}/api/stats/v1/stats-user-personal/${userGuid}`, { headers: this.constants.getHeaders() });
  }

  searchByDate(fromDate: any, toDate: any): Observable<any>{
    return this.http.get(`${baseUrl}/api/cleaning/v1/search-by-date/${fromDate}/${toDate}`, { headers: this.constants.getHeaders() });
  }

  public searchCleaningLogsByDateKeyValue(dateFrom: any, dateTo: any,key: any, value: any, type: any): Observable<any> {
    return this.http.get(`${baseUrl}/api/cleaning/v1/search-by-date-and-key-value/${dateFrom}/${dateTo}/${key}/${value}/${type}`, { headers: this.constants.getHeaders() });
  }

  getStats(): Observable<any> {
    return this.http.get(`${baseUrl}/api/stats/v1/stats-all-cleanings`, { headers: this.constants.getHeaders() });
  }

  getCardStats(): Observable<any> {
    return this.http.get(`${baseUrl}/api/stats/v1/stats`, { headers: this.constants.getHeaders() });
  }

  getTodayGraphStats(): Observable<any> {
    return this.http.get(`${baseUrl}/api/stats/v1/stats-today-cleanings`, { headers: this.constants.getHeaders() });
  }
}
