import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})

export class SiteVisitService {

  constructor(private http: HttpClient, private constants: ConstantsService) { }

  public getSites(): Observable<any> {
    return this.http.get(`${baseUrl}/api/assessments/v1/list-all/false`, { headers: this.constants.getHeaders() });
  }

  public getAnswers(assessmentGuid: any): Observable<any> {
    return this.http.get(`${baseUrl}/api/assessments-answer/v1/search/assessmentGuid/${assessmentGuid}`, { headers: this.constants.getHeaders() });
  }
}
