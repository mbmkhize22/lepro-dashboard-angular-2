import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExcelServiceService {

  //https://www.ngdevelop.tech/export-to-excel-in-angular-6/
  //https://www.tektutorialshub.com/angular/how-to-export-to-excel-in-angular/
  constructor() { }

  public generateExcelFile(headers: any[] ,data: any[], title: any, reportName: any) {
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("report");
    let headerRow = worksheet.addRow(headers);
    worksheet.addRows(data);
    workbook.xlsx.writeBuffer().then((res) => {
      let blob = new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, `${reportName}.xlsx`);
    });
  }
}
