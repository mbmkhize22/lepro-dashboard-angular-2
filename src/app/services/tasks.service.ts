import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { ConstantsService } from './constants.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(private http: HttpClient, private tokenStorageService: TokenService, private constants: ConstantsService) { }

  // List All
  public listAll(): Observable<any> {
    return this.http.get(`${baseUrl}/api/task/v2`, { headers: this.constants.getHeaders() });
  }

  // Search
  public search(key: any, value: any): Observable<any> {
    return this.http.get(`${baseUrl}/api/task/v2/search-key-value/${key}/${value}`, { headers: this.constants.getHeaders() });
  }

  // Save Task
  public save(data: any): Observable<any> {
    return this.http.post(`${baseUrl}/api/task/v2`, data, { headers: this.constants.getHeaders() });
  }

  // Delete Task Activity
  public deleteAtmFromTask(taskActivityId: any): Observable<any> {
    return this.http.delete(`${baseUrl}/api/task-activity/v2/${taskActivityId}`, { headers: this.constants.getHeaders() });
  }

  // Delete Task
  public deleteTask(taskId: any): Observable<any> {
    return this.http.delete(`${baseUrl}/api/task/v2/${taskId}`, { headers: this.constants.getHeaders() });
  }
}
