import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CleaningLogsComponent } from './pages/cleaning-logs/cleaning-logs.component';
import { LiveLogsComponent } from './pages/live-logs/live-logs.component';
import { ATMsComponent } from './pages/atms/atms.component';
import { UsersComponent } from './pages/users/users.component';
import { TestComponent } from './pages/test/test.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { ErrorComponent } from './pages/error/error.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { HeaderComponent } from './components/header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StatCardComponent } from './components/stat-card/stat-card.component';
import { SiteVisitsComponent } from './pages/site-visits/site-visits.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { JobCardsComponent } from './pages/job-cards/job-cards.component';


@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent,
    SideNavComponent,
    FooterComponent,
    LoginComponent,
    DashboardComponent,
    CleaningLogsComponent,
    LiveLogsComponent,
    ATMsComponent,
    UsersComponent,
    TestComponent,
    ClientsComponent,
    ErrorComponent,
    BreadcrumbComponent,
    HeaderComponent,
    StatCardComponent,
    SiteVisitsComponent,
    TasksComponent,
    JobCardsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NgbModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
