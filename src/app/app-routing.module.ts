import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { ATMsComponent } from './pages/atms/atms.component';
import { CleaningLogsComponent } from './pages/cleaning-logs/cleaning-logs.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ErrorComponent } from './pages/error/error.component';
import { JobCardsComponent } from './pages/job-cards/job-cards.component';
import { LiveLogsComponent } from './pages/live-logs/live-logs.component';
import { LoginComponent } from './pages/login/login.component';
import { SiteVisitsComponent } from './pages/site-visits/site-visits.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { TestComponent } from './pages/test/test.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
  {
    path: 'main', component: HeaderComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'users', component: UsersComponent },
      { path: 'tasks', component: TasksComponent },
      { path: 'job-cards', component: JobCardsComponent },
      { path: 'live', component: LiveLogsComponent },
      { path: 'cleaning-logs', component: CleaningLogsComponent },
      { path: 'clients', component:  ClientsComponent},
      { path: 'atms', component: ATMsComponent },
      { path: 'site-visits', component: SiteVisitsComponent }
    ]
  },
  { path: 'test', component: TestComponent },
  { path: 'login', component: LoginComponent },
  { path: '404', component: ErrorComponent },
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
