import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/api.service';
import { ConstantsService } from 'src/app/services/constants.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginFormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  })

  token!: string
  
  constructor(
    private router: Router,
    private apiService: ApiService,
    private tokenStorage: TokenService,
    private constantsService: ConstantsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.tokenStorage.signOut();
  }

  onLogin() {
    if (this.loginFormGroup.valid) {
      this.spinner.show();
      this.apiService.login(this.loginFormGroup.value).subscribe((results) => {
        this.spinner.hide();
        console.log(results); 
        console.log();
        if (results.responseCode == 401) {
          this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
        }else if (results.responseCode == 200) {
          this.tokenStorage.saveToken(results.results.token);
          this.tokenStorage.saveUser(results.results.user);
          this.router.navigateByUrl('main/dashboard');
        }else{
          this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
        }
      }, error => {
        this.spinner.hide();
        if (error.status == 401) {
          this.constantsService.notify(error.error.responseMessage, this.constantsService.DANGER);
        } else {
          this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
        }
      })
    } else {
      this.constantsService.notify('Email/Username & Password is required!!!', this.constantsService.DANGER);
    }
  }

}
