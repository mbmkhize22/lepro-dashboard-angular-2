import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CleaningLogsComponent } from './cleaning-logs.component';

describe('CleaningLogsComponent', () => {
  let component: CleaningLogsComponent;
  let fixture: ComponentFixture<CleaningLogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CleaningLogsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CleaningLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
