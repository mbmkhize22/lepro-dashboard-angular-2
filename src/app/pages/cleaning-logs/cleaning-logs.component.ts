import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CleaningLogService } from 'src/app/services/cleaning-log.service';
import { ConstantsService } from 'src/app/services/constants.service';
import { TokenService } from 'src/app/services/token.service';
import { googleApiKey } from 'src/environments/environment';
import { Loader } from "@googlemaps/js-api-loader";
import { UserService } from 'src/app/services/user.service';
import { ExcelServiceService } from 'src/app/services/excel-service.service';

declare var $: any;
declare function loadDateOnPageChange(): any;

@Component({
  selector: 'app-cleaning-logs',
  templateUrl: './cleaning-logs.component.html',
  styleUrls: ['./cleaning-logs.component.css']
})
export class CleaningLogsComponent implements OnInit {

  liveList!: any;

  page: number = 1;
  totalRecords!: any;
  searchText!: string;

  atmId: any;
  dateCaptured: any;
  atmCondition: any;
  areaName: any;
  latitude: any;
  longitude: any;
  commentBefore: any;
  ccommentAfter: any;
  picList: any = [];
  status!: any;
  fullName: any;
  moreInfo: any;
  loader: any;
  cleaner: any;
  selectedSearchType: any = "atm_ID_Code";
  selectedSearchType1: any = "contains";
  fromDate: any;
  toDate: any;
  responseMessage: any;

  constructor(
    private router: Router,
    private cleaningLogService: CleaningLogService,
    private userService: UserService,
    private tokenStorage: TokenService,
    private constantsService: ConstantsService,
    private spinner: NgxSpinnerService,
    private excelServiceService: ExcelServiceService
  ) { }

  ngOnInit(): void {
    loadDateOnPageChange();
    this.loadAllCleaningLogs();
    this.loader = new Loader({
      apiKey: `${googleApiKey}`
    });
  }

  dowloadReport() {
    const headers = ['Cleaner', 'ATM', 'Condition', 'Date', 'Finish Time', 'Status', 'Area Name', 'Province', 'Latitude', 'Longitude', 'Comments Before', 'Comments After', 'App Version', 'Phone Os'];
    var main_list = [];
    for (var i in this.liveList) {
      var list = [];
      list.push(this.liveList[i].cleaningLog.cleanerFullname);
      list.push(this.liveList[i].cleaningLog.atm_ID_Code);
      list.push(this.liveList[i].cleaningLog.atmCondition);
      list.push(this.liveList[i].cleaningLog.dateAdded);
      list.push(this.liveList[i].cleaningLog.dateUpdated);
      list.push(this.liveList[i].cleaningLog.status);
      list.push(this.liveList[i].cleaningLog.actualFullAddress);
      list.push(this.liveList[i].cleaningLog.province);
      list.push(this.liveList[i].cleaningLog.latitude);
      list.push(this.liveList[i].cleaningLog.longitude);
      list.push(this.liveList[i].cleaningLog.beforeComments);
      list.push(this.liveList[i].cleaningLog.afterComments);
      list.push(this.liveList[i].cleaningLog.appVersion);
      list.push(this.liveList[i].cleaningLog.phoneOs);
      main_list.push(list); 
    }
    this.excelServiceService.generateExcelFile(headers, main_list, "Cleaning Log List", "CleaningLogListReport");
  }

  open(row: any) {
    //console.log(row);
    this.clear();
    this.atmId = row.cleaningLog.atm_ID_Code;
    this.dateCaptured = row.cleaningLog.dateAdded + " => " + row.cleaningLog.dateUpdated;
    this.atmCondition = row.cleaningLog.atmCondition;
    this.areaName = row.cleaningLog.actualFullAddress;
    this.latitude = row.cleaningLog.latitude;
    this.longitude = row.cleaningLog.longitude;
    this.commentBefore = row.cleaningLog.beforeComments;
    this.ccommentAfter = row.cleaningLog.afterComments;
    this.status = row.cleaningLog.status;
    this.fullName = row.cleaningLog.cleanerFullname;
    this.moreInfo = row.cleaningLog;
    this.getCleaner(row.cleaningLog.userGuid);

    const myLatLng = { lat: +row!.cleaningLog.latitude, lng: +row!.cleaningLog.longitude };
    this.loader.load().then(() => {
      const map = new google.maps.Map(document.getElementById("map")!, {
        center: myLatLng,
        zoom: 6
      });
      new google.maps.Marker({
        position: myLatLng,
        map,
        title: "Hello World!",
      });
    });

    for (let i of row.cleaningImages) {
      this.picList.push(i);
    }
    
  }

  search() {
    this.spinner.show();
    this.cleaningLogService.searchCleaningLogs(this.selectedSearchType, this.searchText, this.selectedSearchType1).subscribe(results => {
      if (results.responseCode == 200) {
        this.liveList = results.results;
        this.totalRecords = this.liveList.length;
        this.responseMessage = results.responseMessage;
        console.log(this.liveList);
      }else{
        console.log(results);
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      
      this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  searchByDateRange() {
    this.page = 1;
    let selectedDate = $('#reportDate').val();
    this.fromDate = selectedDate.substring(0, 10);
    this.toDate = selectedDate.substring(13, 23);

    this.spinner.show();
    this.cleaningLogService.searchByDate(this.fromDate, this.toDate).subscribe(results => {
      if (results.responseCode == 200) {
        this.liveList = results.results;
        this.totalRecords = this.liveList.length;
        this.responseMessage = results.responseMessage;
        //console.log(this.liveList);
      } else {
        //console.log(results);
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }

      this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });

  }

  combineSearch() {
    this.page = 1;
    let selectedDate = $('#reportDate').val();
    this.fromDate = selectedDate.substring(0, 10);
    this.toDate = selectedDate.substring(13, 23);

    this.spinner.show();
    this.cleaningLogService.searchCleaningLogsByDateKeyValue(this.fromDate, this.toDate, this.selectedSearchType, this.searchText, this.selectedSearchType1).subscribe(results => {
      if (results.responseCode == 200) {
        this.liveList = results.results;
        this.totalRecords = this.liveList.length;
        this.responseMessage = results.responseMessage;
        //console.log(this.liveList);
      } else {
        //console.log(results);
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }

      this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });

  }

  clear() {
    this.atmId = "";
    this.dateCaptured = "";
    this.atmCondition = "";
    this.areaName = "";
    this.latitude = "";
    this.longitude = "";
    this.commentBefore = "";
    this.ccommentAfter = "";
    this.picList = [];
    this.status = "";
    this.cleaner = null;
  }

  loadAllCleaningLogs() {
    this.searchText = "";
    this.spinner.show();
    this.cleaningLogService.getAllCleaningLogs().subscribe(results => {
      console.log(this.liveList);
      this.liveList = results.results;
      this.totalRecords = this.liveList.length;
      this.responseMessage = results.responseMessage;
      console.log(this.liveList);
      this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  getCleaner(userGuid: any) {
    this.searchText = "";
    this.spinner.show();
    this.userService.searchUserByGuid(userGuid).subscribe(results => {
      //console.log(results);
      this.cleaner = results.results;
      this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

}
