import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ATMsComponent } from './atms.component';

describe('ATMsComponent', () => {
  let component: ATMsComponent;
  let fixture: ComponentFixture<ATMsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ATMsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ATMsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
