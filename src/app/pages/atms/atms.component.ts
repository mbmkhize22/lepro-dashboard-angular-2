import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstantsService } from 'src/app/services/constants.service';
import { TokenService } from 'src/app/services/token.service';
import { AtmService } from 'src/app/services/atm.service';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { AtmModel } from './atms.component.model';
import { ExcelServiceService } from 'src/app/services/excel-service.service';
//import { ExcelServiceService } from 'src/app/services/excel-service.service';

@Component({
  selector: 'app-atms',
  templateUrl: './atms.component.html',
  styleUrls: ['./atms.component.css']
})
export class ATMsComponent implements OnInit {

  //atmForm !: FormGroup;
  atmModelObj : AtmModel = new AtmModel();
  //formBuilder: FormBuilder = new FormBuilder()
  searchText!: string;
  atmList!:any;
  atmData !: any;
  selectedSearchType = "atmName";
  selectedSearchType1 = "contains";


  page: number = 1;
  totalRecords!: any;
  api: any;
  newAtm!: boolean;

  atmForm = new FormGroup({
    atmId: new FormControl(''),
    atmGuid: new FormControl(''),
    atmName: new FormControl('', [Validators.required]),
    atm_ID_Code: new FormControl('', [Validators.required]),
    city: new FormControl('', [Validators.required]),
    clientGuid: new FormControl('', [Validators.required]),
    streetAddress1: new FormControl('', [Validators.required]),
    streetAddress2: new FormControl(''),
    suburb: new FormControl('', [Validators.required]),
    province: new FormControl('', [Validators.required])
  });
  
  responseMessage: any;

  constructor(
    formBuilder: FormBuilder,
    private router: Router,
    private atmService: AtmService,
    private tokenStorage: TokenService,
    private constantsService: ConstantsService,
    private spinner: NgxSpinnerService,
    private excelServiceService: ExcelServiceService
      ) { }

  ngOnInit(): void {
    this.loadAtms();
   }

   search() {
     console.log("Search Key", this.selectedSearchType);
     console.log("Search Type", this.selectedSearchType1);
     console.log("Search Value", this.searchText);
     this.spinner.show();
     this.atmService.searchAtm(this.selectedSearchType, this.searchText, this.selectedSearchType1).subscribe(results => {
       this.atmList = results.results;
       this.totalRecords = this.atmList.length;
       //console.log(this.atmList);
       this.responseMessage = results.responseMessage;
       this.spinner.hide();

     }, error => {
       if (error.status == 401) {
         this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
         this.router.navigateByUrl('/');
       } else {
         this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
       }
       this.spinner.hide();
     });
   }

  clear() {
    this.atmForm.reset();
    this.newAtm = true;
  }

  dowloadReport() {
    const headers = ['ATM ID', 'NAME', 'Address1', 'Address2', 'Province', 'City', 'Suburb'];
    var main_list = [];
    for (var i in this.atmList) {
      var list = [];
      list.push(this.atmList[i].atm_ID_Code);
      list.push(this.atmList[i].atmName);
      list.push(this.atmList[i].streetAddress1);
      list.push(this.atmList[i].streetAddress2);
      list.push(this.atmList[i].province);
      list.push(this.atmList[i].city);
      list.push(this.atmList[i].suburb);
      main_list.push(list);
    }
    this.excelServiceService.generateExcelFile(headers, main_list, "ATM List", "atmListReport");
  }

  openATM(row: any) {
    this.atmForm.reset();
    this.newAtm = false; 
    this.atmForm.controls['atmGuid'].setValue(row.atmGuid);
    this.atmForm.controls['atmId'].setValue(row.atmId);
    this.atmForm.controls['atmName'].setValue(row.atmName);
    this.atmForm.controls['atm_ID_Code'].setValue(row.atm_ID_Code);
    this.atmForm.controls['city'].setValue(row.city);
    this.atmForm.controls['streetAddress1'].setValue(row.streetAddress1);
    this.atmForm.controls['streetAddress2'].setValue(row.streetAddress2);
    this.atmForm.controls['suburb'].setValue(row.suburb);
    this.atmForm.controls['province'].setValue(row.province);
    console.log(this.atmForm.value);
  }
   
   postAtmDetails() {
    this.spinner.show();

    this.atmForm.controls['clientGuid'].setValue('d2f3ffcc-27aa-449b-b367-da09db084be4');
     //console.log(this.atmForm.value);
    this.atmService.postAtm(this.atmForm.value).subscribe(results => {
    this.spinner.hide();
      console.log(results);

       if (results !== null && results.responseCode === 201) {
         this.constantsService.notify(results.responseMessage, this.constantsService.SUCCESS);
         this.loadAtms();
       }else{
         this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
       }

      this.atmForm.reset();
      let ref = document.getElementById('cancel')
      ref?.click();
       
     }, error => {
       if (error.status == 401) {
         this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
         this.router.navigateByUrl('/');
       } else {
         console.log(error);
         this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
       }
       this.spinner.hide();
     });
  }

  updateAtmDetails() {
    if (confirm(`Are you sure you want to save changes for ATM ${this.atmForm.value.atm_ID_Code}?`)) {
    this.spinner.show();

    this.atmForm.controls['clientGuid'].setValue('d2f3ffcc-27aa-449b-b367-da09db084be4');
    this.atmService.updateAtm(this.atmForm.value).subscribe(results => {
      this.spinner.hide();
      console.log(results);

      if (results !== null && results.responseCode === 200) {
        this.constantsService.notify(results.responseMessage, this.constantsService.SUCCESS);
        this.loadAtms();
      } else {
        this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
      }

      this.atmForm.reset();
      let ref = document.getElementById('cancel')
      ref?.click();

    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        console.log(error);
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
    }
  }

  deleteAtm() {
    if (confirm(`Are you sure you want to delete ATM ${this.atmForm.value.atm_ID_Code}?`)) {
      
    this.spinner.show();

    this.atmForm.controls['clientGuid'].setValue('d2f3ffcc-27aa-449b-b367-da09db084be4');
    console.log("UPDATE:", this.atmForm.value);
    this.atmService.deleteAtm(this.atmForm.value).subscribe(results => {
      this.spinner.hide();
      console.log(results);

      if (results !== null && results.responseCode === 200) {
        this.constantsService.notify(results.responseMessage, this.constantsService.SUCCESS);
        this.loadAtms();
      } else {
        this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
      }

      this.atmForm.reset();
      let ref = document.getElementById('cancel')
      ref?.click();

    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        console.log(error);
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
    }
  }


  loadAtms() {
    this.spinner.show();
    this.atmService.getAllAtm().subscribe(results => {
      this.atmList = results.results;
      this.totalRecords = this.atmList.length;
      this.responseMessage = results.responseMessage;
      this.spinner.hide();
    
    }, error => {
        if (error.status == 401) {
          this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
          this.router.navigateByUrl('/');
        } else {
          this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
        }
        this.spinner.hide();
      });
    }
  }




