export class AtmModel{
    [x: string]: any;
    atmName : string = '';
    atm_ID_Code : number = 0;
    city : string = '';
    streetAddress1 : string = '';
    streetAddress2 : string = '';
    suburb : string = '';
    
}