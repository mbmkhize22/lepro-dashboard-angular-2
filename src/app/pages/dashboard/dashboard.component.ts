import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CleaningLogService } from 'src/app/services/cleaning-log.service';
import { ConstantsService } from 'src/app/services/constants.service';
import { ExcelServiceService } from 'src/app/services/excel-service.service';
import { TokenService } from 'src/app/services/token.service';
import { UserService } from 'src/app/services/user.service';
declare function graph(data: any, divTagId: any, chartType: any, yAxisLabel: any, title: any, subTitle: any, seriesName: any): any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  totStatGraph: any = [];
  id: any;
  totalNumberOfAtms: any = 0;
  totalNumberOfUsers: any = 0;
  totalNumberOfCompletedAtms: any = 0;
  totalNumberOfPendingAtms: any = 0;

  constructor(
    private router: Router,
    private cleaningLogService: CleaningLogService,
    private userService: UserService,
    private tokenStorage: TokenService,
    private constantsService: ConstantsService,
    private spinner: NgxSpinnerService,
    private excelServiceService: ExcelServiceService
  ) { }

  ngOnInit(): void {
    this.loadStats();
    this.loadCardStats();
    this.id = setInterval(() => {
      this.loadStats();
      this.loadCardStats();
    }, 120000); //runs every 2 min
  }

  ngOnDestroy() {
    if (this.id) {
      clearInterval(this.id);
    }
  }

  loadCardStats() {
    //this.spinner.show();
    this.cleaningLogService.getCardStats().subscribe(results => {
      //console.log(results);
      this.totalNumberOfAtms = results.totalNumberOfAtms;
      this.totalNumberOfCompletedAtms = results.totalNumberOfCompletedAtms;
      this.totalNumberOfPendingAtms = results.totalNumberOfPendingAtms;
      this.totalNumberOfUsers = results.totalNumberOfUsers;
      //this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      //this.spinner.hide();
    });
  }

  loadStats() {
    //this.spinner.show();
      this.cleaningLogService.getTodayGraphStats().subscribe(results => {
      this.totStatGraph = [];
      for (let i in results) {
        this.totStatGraph.push([results[i].key, results[i].value]);
      }
      console.log(this.totStatGraph);
      graph(this.totStatGraph, 'tblStatGraph', 'column', 'Total Number of ATM\'s Cleaned', 'Cleaned ATM\'s Chart', 'today', 'Cleaned');

      //this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      //this.spinner.hide();
    });
  }

}
