import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { CleaningLogService } from 'src/app/services/cleaning-log.service';
import { ConstantsService } from 'src/app/services/constants.service';
import { ExcelServiceService } from 'src/app/services/excel-service.service';
import { TasksService } from 'src/app/services/tasks.service';
import { TokenService } from 'src/app/services/token.service';
import { UserService } from 'src/app/services/user.service';
import { UserModel } from '../users/users.component.module';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AtmService } from 'src/app/services/atm.service';
import { Loader } from "@googlemaps/js-api-loader";
import { googleApiKey } from 'src/environments/environment';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  active = 1;

  dueDate: NgbDateStruct | undefined;
  startDate: NgbDateStruct | undefined;

  searchedAtmList: any = [];
  atmList: any = [];
  selectedSearchTypeAtm = "atmName";
  selectedSearchType1Atm = "contains";

  public isCollapsed = false;

  searchAtmTextAtm = "";
  selectedSearchType1 = "";

  selectedSearchType: any;

  pageAtm = 1;
  totalRecordsAtm!: any;
  totalRecordsSelectedAtm!: any;
  pageAtmSize = 4;
  pageSelectedAtm = 1;
  mapOfSelectedAtms = new Map();
  mapOfSelectedAtmsKeys: any;

  browsedAtmList = [];

  searchText!: string;
  userData !: any;
  userList!: any;
  tasksList: any = [];
  userModelObj: UserModel = new UserModel();
  formBuilder: FormBuilder = new FormBuilder()

  page: number = 1;
  pageUserHistory: number = 1;
  totalRecords!: any;
  totalRecordsUserHistory!: any;
  api: any;
  newForm!: boolean;
  userActivityList: any = [];

  userType = "Administrator";
  responseMessage: any;
  responseMessageHistory: any;
  profilePicture: any = "";
  passwordChange: any = "";

  today: any = 0;
  week: any = 0;
  month: any = 0;
  year: any = 0;

  loader: any;

  /**
 * TASK ATTRIBUTES
 */
  selectedSearchTypeTaskSearch = "user.firstName";
  selectedSearchType1TaskSearch = "contains";
  searchTextTaskSearch!: string;
  userListTaskSearch!: any;
  pageTaskSearch: number = 1;
  totalRecordsTaskSearch!: any;
  selectedAtmTask: any;
   /** END TASK ATTRIBUTES */


  /**
   * USERS ATTRIBUTES
   */
  selectedSearchTypeUserSearch = "firstName";
  selectedSearchType1UserSearch = "contains";
  searchTextUserSearch!: string;
  userListUserSearch!: any;
  pageUserSearch: number = 1;
  totalRecordsUserSearch!: any;
  selectedUserId: any;
   /** END USERS ATTRIBUTES */


  taskForm = new FormGroup({
    user: new FormControl('', [Validators.required]),
    taskName: new FormControl(''),
    taskId: new FormControl(''),
    startDate: new FormControl('', [Validators.required]),
    dueDate: new FormControl('', [Validators.required]),
    province: new FormControl('', [Validators.required]),
    frequence: new FormControl('', [Validators.required]),
  });


  constructor(
    private router: Router,
    private tasksService: TasksService,
    private userService: UserService,
    private tokenStorage: TokenService,
    private constantsService: ConstantsService,
    private spinner: NgxSpinnerService,
    private excelServiceService: ExcelServiceService,
    private cleaningLogService: CleaningLogService,
    private atmService: AtmService,
    private config: NgbModalConfig, 
    private modalService: NgbModal

  ) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
   }

  ngOnInit(): void {
    this.loadTasks();
    this.decryptDate('2022-04-21');

    this.loader = new Loader({
      apiKey: `${googleApiKey}`
    });
  }

  getSelectedAtm(isSelected: any, atm: any, id: any) {
    if(isSelected == true) {
      this.mapOfSelectedAtms.set(id, atm);
    }else{
      this.mapOfSelectedAtms.delete(id);
    }
    //console.log(isSelected, atm, id);
    console.log(this.mapOfSelectedAtms);
    this.totalRecordsSelectedAtm = this.mapOfSelectedAtms.size;
    this.mapOfSelectedAtmsKeys = Array.from(this.mapOfSelectedAtms.keys());
    console.log(this.mapOfSelectedAtmsKeys);
  }

  deleteTaskAtm(id: any) {
    console.log(id);
    if (confirm(`Delete this ATM from this task?`) == true) {
      console.log(this.mapOfSelectedAtms.get(id));
      var taskActivityId = this.mapOfSelectedAtms.get(id).activityDetails.taskActivityId;
      this.deleteTaskActivity(taskActivityId); // deleting from Database online
      console.log(taskActivityId);
      this.mapOfSelectedAtms.delete(id); // delete key from Hash Map object
      var i = this.mapOfSelectedAtmsKeys.indexOf(id); // Getting the position of the value to be deleted on the array
      this.mapOfSelectedAtmsKeys.splice(i, 1); // Deleting the array element using index .splice(start index, length)
      console.log(this.mapOfSelectedAtmsKeys);
      
    }
  }

  deleteTaskAtmServer(atmId: any, taskActivityId: any) {
    if (confirm(`Delete this ATM from this task?`) == true) {
      this.mapOfSelectedAtms.delete(atmId); // delete key from Hash Map object
      var i = this.mapOfSelectedAtmsKeys.indexOf(atmId); // Getting the position of the value to be deleted on the array
      this.mapOfSelectedAtmsKeys.splice(i, 1); // Deleting the array element using index .splice(start index, length)
    }
  }

  // Delete the main task
  deleteTask(id: any, name: any, surname: any) {
    console.log(id);
    if (confirm(`Delete this Task for ${name} ${surname}?`) == true) {
      console.log(id);
      this.deleteTaskFromServer(id);
      this.loadTasks();
    }
  }

  getSelected(isSelected: any, product: any) {
    console.log(isSelected, product)
  }


  open(content: any) {
    this.clearTaskScreen();
    this.modalService.open(content, { size: 'xl' });
  }

  openAtmModal(content: any) {
    this.clearAtmScreen();
    this.modalService.open(content, { size: 'xl' });
  }

  openAtmTaskModal(content: any, data: any) {
    //console.log(data);
    this.selectedAtmTask = data['activityDetails'];
    console.log(this.selectedAtmTask);
    //this.clearAtmScreen();

    /**
     * MAP
     */
    const myLatLng = { lat: +this.selectedAtmTask!.latitude, lng: +this.selectedAtmTask!.longitude };
    this.loader.load().then(() => {
      const map = new google.maps.Map(document.getElementById("map")!, {
        center: myLatLng,
        zoom: 6
      });
      new google.maps.Marker({
        position: myLatLng,
        map,
        title: "Hello World!",
      });
    });
    this.modalService.open(content, { size: 'xl' });
  }


  openUsersModal(content: any) {
    this.modalService.open(content, { size: 'xl' });
  }

  decryptDate(date: any) {
    const [year, month, day] = date.split('-');
    const obj = {
      year: parseInt(year),
      month: parseInt(month),
      day: parseInt(day)
    };
    return obj;
  }

  openBrowse(content: any, row: any) {
    this.clearTaskScreen();
    console.log(row);
    console.log(row.taskActivities);
    this.taskForm.controls['taskName'].setValue(row.taskName);
    this.taskForm.controls['province'].setValue(row.region);
    this.taskForm.controls['user'].setValue(row.user.firstName + ' ' + row.user.lastName);
    this.selectedUserId = row.user.userId;
    this.taskForm.controls['taskId'].setValue(row.taskId);
    this.taskForm.controls['dueDate'].setValue(this.decryptDate(row.dueDate));
    this.taskForm.controls['startDate'].setValue(this.decryptDate(row.startDate));
    this.taskForm.controls['frequence'].setValue(row.frequency);

    // setting up atm list
    for (var activity of row.taskActivities){
      //set up keys
      //this.mapOfSelectedAtmsKeys.push(activity.atm.atmId);
      this.mapOfSelectedAtmsKeys.push(activity.atm.atmId);
      // set Map object
      activity.atm['activityDetails'] = activity;
      console.log(activity);
      //this.mapOfSelectedAtms.set(activity.atm.atmId, activity.atm);
      this.mapOfSelectedAtms.set(activity.atm.atmId, activity.atm);
    }

    this.modalService.open(content, { size: 'xl' });
  }

  loadAtms() {
    this.atmList = [];
  }

  clearAtmScreen() {
    this.atmList = [];
    this.pageAtm = 1;
    this.totalRecordsAtm = 0;
  }

  clearTaskScreen() {
    this.clearAtmScreen();
    this.taskForm.reset();
    this.selectedUserId = null;
    this.mapOfSelectedAtmsKeys = [];
    this.mapOfSelectedAtms.clear();
    this.totalRecordsSelectedAtm = 0;
    this.pageSelectedAtm = 1;
  }

  searchAtm() {
    console.log("Search Key", this.selectedSearchTypeAtm);
    console.log("Search Type", this.selectedSearchType1Atm);
    console.log("Search Value", this.searchAtmTextAtm);
    this.spinner.show();
    this.atmService.searchAtm(this.selectedSearchTypeAtm, this.searchAtmTextAtm, this.selectedSearchType1Atm).subscribe(results => {
      this.atmList = results.results;
      this.totalRecordsAtm = this.atmList.length;
      this.pageAtm = 1;
      console.log(this.atmList);
      this.responseMessage = results.responseMessage;
      this.spinner.hide();

    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  dowloadReport() {
    const headers = ['Surname', 'Name', 'Mobile', 'Email', 'Role', 'lastLoginDate'];
    var main_list = [];
    for (var i in this.userList) {
      var list = [];
      list.push(this.userList[i].user.lastName);
      list.push(this.userList[i].user.firstName);
      list.push(this.userList[i].contact.mobile);
      list.push(this.userList[i].user.username);
      list.push(this.userList[i].user.userType);
      list.push(this.userList[i].user.lastLoginDate);
      main_list.push(list);
    }
    this.excelServiceService.generateExcelFile(headers, main_list, "User List", "UserListReport");
  }

  search() {
    this.spinner.show();
    this.userService.searchUsers(this.selectedSearchType, this.searchText, this.selectedSearchType1).subscribe(results => {
      this.userList = results.results;
      this.totalRecords = this.userList.length;
      //console.log(this.userList);
      this.responseMessage = results.responseMessage;
      this.spinner.hide();
    }, error => {
      //console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  getPersonalStat(userGuid: any) {
    this.cleaningLogService.getPersonalStats(userGuid).subscribe(results => {
      console.log(results);
      this.today = results.today;
      this.month = results.month;
      this.week = results.week;
      this.year = results.year;
    }, error => {
      //console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
    });
  }

  cleanDate(date: any) {
    return `${date.year}-${(date.month < 10 ? '0' : '') + date.month}-${(date.day < 10 ? '0' : '') + date.day}`
  }

  save() {
    //console.log(this.taskForm.value);
    this.spinner.show();

    var atmAcvitityList = [];
    for (var x of this.mapOfSelectedAtmsKeys) {
      atmAcvitityList.push(
        {
          "atm": {
            "atmId": x
          }
        }
      );
    }
    //console.log(atmAcvitityList);
    var data_update = {
      "taskId": this.taskForm.value.taskId,
      "dueDate": this.cleanDate(this.taskForm.value.dueDate),
      "region": this.taskForm.value.province,
      "startDate": this.cleanDate(this.taskForm.value.startDate),
      "taskActivities": atmAcvitityList,
      "frequency": this.taskForm.value.frequence,
      "taskName": this.taskForm.value.taskName,
      "user": {
        "userId": this.selectedUserId
      }
    };

    var  data_new = {
        "dueDate": this.cleanDate(this.taskForm.value.dueDate),
        "region": this.taskForm.value.province,
        "startDate": this.cleanDate(this.taskForm.value.startDate),
        "taskActivities": atmAcvitityList,
        "taskName": this.taskForm.value.taskName,
        "frequency": this.taskForm.value.frequence,
        "user": {
          "userId": this.selectedUserId
        }
      };

    // Deciding to either create new record or update exising
    var data = this.taskForm.value.taskId == null ? data_new : data_update;
    console.log('DATA', data);

    //console.log(data);
    this.tasksService.save(data).subscribe(results => {
      this.spinner.hide();
      console.log(results);
      this.constantsService.notify('Task created successfully', this.constantsService.SUCCESS);
      this.loadTasks();
      this.clearTaskScreen();
    
      /**if (results !== null && results.responseCode === 201) {
        this.constantsService.notify(results.responseMessage, this.constantsService.SUCCESS);
        this.loadTasks();
      } else {
        this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
      }*/

      this.taskForm.reset();
      this.clearTaskScreen();
      let ref = document.getElementById('cancel')
      ref?.click();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        console.log(error);
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();

    });
  }

  loadTasks() {
    this.searchText = "";
    this.searchTextTaskSearch = "";
    this.spinner.show();
    this.tasksService.listAll().subscribe(results => {
      //console.log(results);
      this.page = 1;
      this.tasksList = results;
      /*this.userList = results.results;
      this.totalRecords = this.userList.length;
      console.log(this.userList);
      this.responseMessage = results.responseMessage;*/
      this.spinner.hide();
    }, error => {
      console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  searchTasks() {
    this.spinner.show();
    this.tasksService.search(this.selectedSearchTypeTaskSearch, this.searchTextTaskSearch).subscribe(results => {
      //console.log(results);
      this.page = 1;
      this.tasksList = results;
      this.spinner.hide();
    }, error => {
      console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  deleteTaskActivity(taskActivityId: any) {
    this.spinner.show();
    this.tasksService.deleteAtmFromTask(taskActivityId).subscribe(results => {
      this.constantsService.notify('ATM Deleted successfully', this.constantsService.SUCCESS);
      this.spinner.hide();
    }, error => {
      console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  deleteTaskFromServer(id: any) {
    this.spinner.show();
    this.tasksService.deleteTask(id).subscribe(results => {
      this.constantsService.notify('Task Deleted successfully', this.constantsService.SUCCESS);
      this.spinner.hide();
    }, error => {
      console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  calcProgress(array_: []) {
    let totalItems = array_.length;
    let totalComplete = 0;
    let percentage = 0;
    //console.log(totalItems);
    for (let t of array_) {
      //console.log(t['status']);
      if (t['status'] == 'COMPLETED') {
        totalComplete ++;
      }
    }
    percentage = Math.ceil((totalComplete / totalItems) * 100);
    return percentage;
  }

  calcStatus(val: any) {
    if(val == 0) {
      return 'PENDING';
    }else if(val == 100) {
      return 'COMPLETED';
    }else{
      return 'IN PROGRESS';
    }
  }

  /** USER FUNCTIONS */
  searchUsers() {
    this.spinner.show();
    this.userService.searchUsers(this.selectedSearchTypeUserSearch, this.searchTextUserSearch, this.selectedSearchType1UserSearch).subscribe(results => {
      this.userListUserSearch = results.results;
      this.totalRecordsUserSearch = this.userListUserSearch.length;
      //console.log(this.userList);
      this.spinner.hide();
    }, error => {
      //console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  selectUser(userId: any, firstName: any, lastName: any) {
    this.taskForm.controls['user'].setValue(firstName + ' ' + lastName);
    this.selectedUserId = userId;
    let ref = document.getElementById('cancelUserSearchModal');
    ref?.click();
  }
/** END USER FUNCTIONS */



}


