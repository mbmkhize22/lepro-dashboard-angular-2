import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstantsService } from 'src/app/services/constants.service';
import { SiteVisitService } from 'src/app/services/site-visit.service';

@Component({
  selector: 'app-site-visits',
  templateUrl: './site-visits.component.html',
  styleUrls: ['./site-visits.component.css']
})
export class SiteVisitsComponent implements OnInit {

  liveList!: any;
  questions: any = [];

  page: number = 1;
  totalRecords!: any;

  currentData: any;


  constructor(private spinner: NgxSpinnerService, private siteVisit: SiteVisitService, private constants: ConstantsService, private router: Router) { }

  ngOnInit(): void {
    this.loadSites();
  }

  open(item: any) {
    console.log(item);
    this.currentData = item;
    this.loadQuestions(item.assessment.assessmentGuid);
  }

  dowloadReport() {

  }

  loadSites() {
    this.spinner.show();
    this.siteVisit.getSites().subscribe(results => {
      console.log(results);
      this.liveList = results.results;
      this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constants.notify(error.statusText + ", session expired! ", this.constants.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constants.notify('System error occurred, please contact Administrator!', this.constants.DANGER);
      }
      this.spinner.hide();
    });
  }

  loadQuestions(assetmentGuid: any) {
    // this.spinner.show();
    this.siteVisit.getAnswers(assetmentGuid).subscribe(results => {
      //console.log(results);
      this.questions = results.results;
      //console.log('this.questions', this.questions);
      // this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constants.notify(error.statusText + ", session expired! ", this.constants.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constants.notify('System error occurred, please contact Administrator!', this.constants.DANGER);
      }
      // this.spinner.hide();
    });
  }

  print() {
    window.print();
  }

}
