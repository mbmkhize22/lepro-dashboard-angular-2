import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-job-cards',
  templateUrl: './job-cards.component.html',
  styleUrls: ['./job-cards.component.css']
})
export class JobCardsComponent implements OnInit {


  page: any = 1;

/**
* JOB CARD ATTRIBUTES
*/
  selectedSearchTypeJobSearch = "user.firstName";
  selectedSearchType1JobSearch = "contains";
  searchTextJobSearch!: string;
  userListJobSearch!: any;
  pageJobSearch: number = 1;
  totalRecordsJobSearch!: any;
  selectedCustomerJob: any;
  tasksList: any = [];
/** END JOB CARD ATTRIBUTES */


/**
* USERS ATTRIBUTES
*/
  selectedSearchTypeUserSearch = "firstName";
  selectedSearchType1UserSearch = "contains";
  searchTextUserSearch!: string;
  userListUserSearch!: any;
  pageUserSearch: number = 1;
  totalRecordsUserSearch!: any;
  selectedUserId: any;
/** END USERS ATTRIBUTES */

  taskForm = new FormGroup({
    user: new FormControl('', [Validators.required]),
    taskName: new FormControl(''),
    taskId: new FormControl(''),
    startDate: new FormControl('', [Validators.required]),
    dueDate: new FormControl('', [Validators.required]),
    province: new FormControl('', [Validators.required]),
    frequence: new FormControl('', [Validators.required]),
  });

  constructor(
    private config: NgbModalConfig,
    private modalService: NgbModal
  ) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
   }

  ngOnInit(): void {
  }

  searchJobs() {

  }

  loadJobs() {

  }

  dowloadReport() {

  }

  open(content: any) {
    //this.clearTaskScreen();
    this.modalService.open(content, { size: 'xl' });
  }

  openAtmModal(content: any) {
    //this.clearAtmScreen();
    this.modalService.open(content, { size: 'xl' });
  }

  openBrowse(content: any, item: any) {

  }

}
