import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Loader } from '@googlemaps/js-api-loader';
import { NgxSpinnerService } from 'ngx-spinner';
import { CleaningLogService } from 'src/app/services/cleaning-log.service';
import { ConstantsService } from 'src/app/services/constants.service';
import { ExcelServiceService } from 'src/app/services/excel-service.service';
import { TokenService } from 'src/app/services/token.service';
import { UserService } from 'src/app/services/user.service';
import { googleApiKey } from 'src/environments/environment';

@Component({
  selector: 'app-live-logs',
  templateUrl: './live-logs.component.html',
  styleUrls: ['./live-logs.component.css']
})

export class LiveLogsComponent implements OnInit {

  liveList!: any;

  page: number = 1;
  totalRecords!: any;
  searchText!: string;
  id!: any;
  atmId: any;
  dateCaptured: any;
  atmCondition: any;
  areaName: any;
  latitude: any;
  longitude: any;
  commentBefore: any;
  ccommentAfter: any;
  picList: any = [];
  status!: any;
  fullName: any;
  moreInfo: any;
  loader: any;
  cleaner: any;

  constructor(
    private router: Router,
    private cleaningLogService: CleaningLogService,
    private tokenStorage: TokenService,
    private constantsService: ConstantsService,
    private spinner: NgxSpinnerService,
    private excelServiceService: ExcelServiceService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.loadLiveLogs();
    this.loader = new Loader({
      apiKey: `${googleApiKey}`
    });
    this.id = setInterval(() => {
      this.loadLiveLogs();
    }, 120000); //runs every 2 min
  }

  ngOnDestroy() {
    if (this.id) {
      clearInterval(this.id);
    }
  }

  clear() {
    this.atmId = "";
    this.dateCaptured = "";
    this.atmCondition = "";
    this.areaName = "";
    this.latitude = "";
    this.longitude = "";
    this.commentBefore = "";
    this.ccommentAfter = "";
    this.picList = [];
    this.status = "";
    this.cleaner = null;
  }

  open(row: any) {
    //console.log(row);
    this.clear();
    this.atmId = row.cleaningLog.atm_ID_Code;
    this.dateCaptured = row.cleaningLog.dateAdded + " => " + row.cleaningLog.dateUpdated;
    this.atmCondition = row.cleaningLog.atmCondition;
    this.areaName = row.cleaningLog.actualFullAddress;
    this.latitude = row.cleaningLog.latitude;
    this.longitude = row.cleaningLog.longitude;
    this.commentBefore = row.cleaningLog.beforeComments;
    this.ccommentAfter = row.cleaningLog.afterComments;
    this.status = row.cleaningLog.status;
    this.fullName = row.cleaningLog.cleanerFullname;
    this.moreInfo = row.cleaningLog;
    this.getCleaner(row.cleaningLog.userGuid);

    const myLatLng = { lat: +row!.cleaningLog.latitude, lng: +row!.cleaningLog.longitude };
    this.loader.load().then(() => {
      const map = new google.maps.Map(document.getElementById("map")!, {
        center: myLatLng,
        zoom: 6
      });
      new google.maps.Marker({
        position: myLatLng,
        map,
        title: "Hello World!",
      });
    });

    for (let i of row.cleaningImages) {
      this.picList.push(i);
    }

  }

  getCleaner(userGuid: any) {
    this.searchText = "";
    this.spinner.show();
    this.userService.searchUserByGuid(userGuid).subscribe(results => {
      //console.log(results);
      this.cleaner = results.results;
      this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

  dowloadReport() {
    const headers = ['Cleaner', 'ATM', 'Condition', 'Date', 'Finish Time', 'Status', 'Area Name', 'Province', 'Latitude', 'Longitude', 'Comments Before', 'Comments After', 'App Version', 'Phone Os'];
    var main_list = [];
    for (var i in this.liveList) {
      var list = [];
      list.push(this.liveList[i].cleaningLog.cleanerFullname);
      list.push(this.liveList[i].cleaningLog.atm_ID_Code);
      list.push(this.liveList[i].cleaningLog.atmCondition);
      list.push(this.liveList[i].cleaningLog.dateAdded);
      list.push(this.liveList[i].cleaningLog.dateUpdated);
      list.push(this.liveList[i].cleaningLog.status);
      list.push(this.liveList[i].cleaningLog.actualFullAddress);
      list.push(this.liveList[i].cleaningLog.province);
      list.push(this.liveList[i].cleaningLog.latitude);
      list.push(this.liveList[i].cleaningLog.longitude);
      list.push(this.liveList[i].cleaningLog.beforeComments);
      list.push(this.liveList[i].cleaningLog.afterComments);
      list.push(this.liveList[i].cleaningLog.appVersion);
      list.push(this.liveList[i].cleaningLog.phoneOs);
      main_list.push(list);
    }
    this.excelServiceService.generateExcelFile(headers, main_list, "Cleaning Log List", "CleaningLiveLogListReport");
  }

  loadLiveLogs() {
    this.searchText = "";
    //this.spinner.show();
    this.cleaningLogService.getLiveLogs().subscribe(results => {
      this.liveList = results.results;
      this.totalRecords = this.liveList.length;
      console.log(this.liveList);
      //this.spinner.hide();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      //this.spinner.hide();
    });
  }

}
