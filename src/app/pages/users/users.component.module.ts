export class UserModel{
    [x: string]: any;
    firstName : string = '';
    lastName : string = '';
    mobile : number = 0;
    email : string = '';
    userType : string = '';
    
}