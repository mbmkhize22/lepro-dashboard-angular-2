import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConstantsService } from 'src/app/services/constants.service';
import { TokenService } from 'src/app/services/token.service';
import { UserService } from 'src/app/services/user.service';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { UserModel } from './users.component.module';
import { ExcelServiceService } from 'src/app/services/excel-service.service';
import { CleaningLogService } from 'src/app/services/cleaning-log.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  
  searchText!: string;
  userData !: any;
  userList!:any;
  userModelObj : UserModel = new UserModel();
  formBuilder: FormBuilder = new FormBuilder()

  page: number = 1;
  pageUserHistory: number = 1;
  totalRecords!: any;
  totalRecordsUserHistory!: any;
  api:any;
  newForm!: boolean;
  userActivityList: any = [];

  selectedSearchType = "firstName";
  selectedSearchType1 = "contains";
  userType = "Administrator";
  responseMessage: any;
  responseMessageHistory: any;
  profilePicture: any = "";
  passwordChange: any = "";

  today: any = 0;
  week: any = 0;
  month: any = 0;
  year: any = 0;


  userForm = new FormGroup({
    userGuid: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl('', [Validators.required]),
    mobile: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    userType: new FormControl('', [Validators.required]),
    streetAddress1: new FormControl('', [Validators.required]),
    province: new FormControl('', [Validators.required]),
  });


  constructor(
    
    private router: Router,
    private userService: UserService,
    private tokenStorage: TokenService,
    private constantsService: ConstantsService,
    private spinner: NgxSpinnerService,
    private excelServiceService: ExcelServiceService,
    private cleaningLogService: CleaningLogService
  
  ) { }

  ngOnInit(): void {
    this.loadUsers();
  }

  dowloadReport() {
    const headers = ['Surname', 'Name', 'Mobile', 'Email', 'Role', 'lastLoginDate'];
    var main_list = [];
    for (var i in this.userList) {
      var list = [];
      list.push(this.userList[i].user.lastName);
      list.push(this.userList[i].user.firstName);
      list.push(this.userList[i].contact.mobile);
      list.push(this.userList[i].user.username);
      list.push(this.userList[i].user.userType);
      list.push(this.userList[i].user.lastLoginDate);
      main_list.push(list);
    }
    this.excelServiceService.generateExcelFile(headers, main_list, "User List", "UserListReport");
  }

  search() {
    this.spinner.show();
    this.userService.searchUsers(this.selectedSearchType, this.searchText, this.selectedSearchType1).subscribe(results => {
      this.userList = results.results;
      this.totalRecords = this.userList.length;
      //console.log(this.userList);
      this.responseMessage = results.responseMessage;
      this.spinner.hide();
    }, error => {
      //console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }
  
  getPersonalStat(userGuid: any) {
    this.cleaningLogService.getPersonalStats(userGuid).subscribe(results => {
      console.log(results);
      this.today = results.today;
      this.month = results.month;
      this.week = results.week;
      this.year = results.year;
    }, error => {
      //console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
    });
  }

  getUserCleaningHistory(key: any, value: any, types: any) {
    //this.spinner.show();
    this.userActivityList = [];
    this.pageUserHistory = 1;
    this.cleaningLogService.searchCleaningLogs(key, value, types).subscribe(results => {
      this.userActivityList = results.results;
      this.totalRecordsUserHistory = this.userActivityList.length;
      this.responseMessageHistory = results.responseMessage;
      //console.log(results);
      //this.spinner.hide();
    }, error => {
      //console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      //this.spinner.hide();
    });
  }

  clear() {
    this.userForm.reset();
    this.newForm = true;
  }

  openUser(row: any) {
    this.profilePicture = "";
    this.userForm.reset();
    this.newForm = false;
    //console.log(row);
    this.userForm.controls['userGuid'].setValue(row.user.userGuid);
    this.userForm.controls['firstName'].setValue(row.user.firstName);
    this.userForm.controls['lastName'].setValue(row.user.lastName);
    this.userForm.controls['email'].setValue(row.contact.email);
    this.userForm.controls['mobile'].setValue(row.contact.mobile);
    this.userForm.controls['userType'].setValue(row.user.userType);
    this.userForm.controls['province'].setValue(row.address.province);
    this.profilePicture = row.user.profilePicture == null ? "" : row.user.profilePicture;
    this.getPersonalStat(row.user.userGuid);
    this.getUserCleaningHistory('userGuid', row.user.userGuid, 'equals');
  }

  //081 558 7027

  postUserDetails() { 
        this.spinner.show();

        this.userService.postUser(this.userForm.value).subscribe(results => {
        this.spinner.hide();
        //console.log(results);

       if (results !== null && results.responseCode === 201) {
         this.constantsService.notify(results.responseMessage, this.constantsService.SUCCESS);
         this.loadUsers();
       }else{
         this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
       }

      this.userForm.reset();
      let ref = document.getElementById('cancel')
      ref?.click();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
       
     });
  }

  changePasswordFunction() {
    this.spinner.show();
    this.userService.changePassword(this.userForm.value['email'], this.passwordChange).subscribe(results => {
      this.spinner.hide();
      if (results !== null && results.responseCode === 200) {
        this.constantsService.notify(results.responseMessage, this.constantsService.SUCCESS);
        this.passwordChange = "";
      } else {
        this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
      }
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();

    });
  }

  UpdateUserDetails() {
    if (confirm(`Are you sure you want to save changes for user ${this.userForm.value.firstName}?`)) {
    this.spinner.show();

    this.userService.updateUser(this.userForm.value).subscribe(results => {
      this.spinner.hide();
      //console.log(results);

      if (results !== null && results.responseCode === 200) {
        this.constantsService.notify(results.responseMessage, this.constantsService.SUCCESS);
        this.loadUsers();
      } else {
        this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
      }

      this.userForm.reset();
      let ref = document.getElementById('cancel2')
      ref?.click();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();

    });
  }
  }

  deleteUser() {
    if (confirm(`Are you sure you want to delete ${this.userForm.value.firstName}?`)) {
    this.spinner.show();

    this.userService.deleteUser(this.userForm.value).subscribe(results => {
      this.spinner.hide();
      //console.log(results);

      if (results !== null && results.responseCode === 200) {
        this.constantsService.notify(results.responseMessage, this.constantsService.SUCCESS);
        this.loadUsers();
      } else {
        this.constantsService.notify(results.responseMessage, this.constantsService.DANGER);
      }

      this.userForm.reset();
      let ref = document.getElementById('cancel')
      ref?.click();
    }, error => {
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();

    });
    }
  }
  
  loadUsers() {
    this.searchText = "";
    this.spinner.show();
    this.userService.getAllUsers().subscribe(results => {
      this.userList = results.results;
      this.totalRecords = this.userList.length;
      console.log(this.userList);
      this.responseMessage = results.responseMessage;
      this.spinner.hide();
    }, error => {
      console.log(error);
      if (error.status == 401) {
        this.constantsService.notify(error.statusText + ", session expired! ", this.constantsService.DANGER);
        this.router.navigateByUrl('/');
      } else {
        this.constantsService.notify('System error occurred, please contact Administrator!', this.constantsService.DANGER);
      }
      this.spinner.hide();
    });
  }

}
