import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  fullname: any;
  jobtitle: any;

  constructor(private tokenStorage: TokenService,) { }

  ngOnInit(): void {
    this.fullname = this.tokenStorage.getUser().firstName + " " + this.tokenStorage.getUser().lastName;
    this.jobtitle = this.tokenStorage.getUser().userType;
  }

}
