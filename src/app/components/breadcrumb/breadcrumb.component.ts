import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  @Input() title: any = "Test Title";
  @Input() subTitle: any = "Test Sub Title";

  constructor() { }

  ngOnInit(): void {
  }

}
