import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  
  fullname: any = '';
  jobtitle: any = '';

  constructor(private tokenStorage: TokenService) { }

  ngOnInit(): void {
    this.fullname = this.tokenStorage.getUser().firstName + " " + this.tokenStorage.getUser().lastName;
    this.jobtitle = this.tokenStorage.getUser().userType;
  }

}
