import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {

  fullname: any = '';
  jobtitle: any = '';

  constructor(private tokenStorage: TokenService) { }

  ngOnInit(): void {
    this.fullname = this.tokenStorage.getUser().firstName + " " + this.tokenStorage.getUser().lastName;
    this.jobtitle = this.tokenStorage.getUser().userType;
  }

}
