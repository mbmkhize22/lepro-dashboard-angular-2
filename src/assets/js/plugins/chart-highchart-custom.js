
function graph(data, divTagId, chartType, yAxisLabel, title, subTitle, seriesName) {
         // [ Donut 3D-pie-chart ] Start
        Highcharts.chart(divTagId, {
            chart: {
                type: chartType,
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 0,
                    depth: 100
                }
            },
            colors: ['#4099ff', '#7759de', '#FF5370', '#FFB64D', '#00bcd4', '#2ed8b6'],
            title: {
                text: title
            },
            subtitle: {
                text: subTitle
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45
                }
            },
            xAxis: {
                categories: data,
                labels: {
                    skew3d: true,
                    style: {
                        fontSize: '10px'
                    }
                }
            },
            yAxis: {
                title: {
                    text: yAxisLabel
                }
            },
            series: [{
                name: seriesName,
                data: data
            }]
        });
        // [ Donut 3D-pie-chart ] end
    }

function myfunction(params1, params2) {
    console.log('param1', params1);
    console.log('param2', params2);
}
