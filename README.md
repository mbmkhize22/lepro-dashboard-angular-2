# LeproDashboardAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.2.

## After cloning project

Run `npm install` this will install all required modules/dependances for the project.

## Development server

Run `ng serve --open` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Required modules

Spinner `npm install ngx-spinner --save` reference [spinner](https://www.npmjs.com/package/ngx-spinner)

Pagination `npm install ngx-pagination --save` reference [pagination](https://www.npmjs.com/package/ngx-pagination)

Bootstrap `ng add @ng-bootstrap/ng-bootstrap` reference [Bootstrap](https://www.npmjs.com/package/@ng-bootstrap/ng-bootstrap)


## Admin Template
reference [template](http://lite.codedthemes.com/flatable/index.html)
[Original tmplate](http://preview.themeforest.net/item/flat-able-bootstrap-4-admin-template/full_screen_preview/19842250)

## Angular Material
- [Angular Material](https://material.angular.io/guide/getting-started)